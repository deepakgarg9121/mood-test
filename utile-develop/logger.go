package utile

import (
	"encoding/json"
	"fmt"
	"os"
	"path"
	"path/filepath"
	"time"

	"go.uber.org/zap"
	"go.uber.org/zap/zapcore"
	"gopkg.in/natefinch/lumberjack.v2"
)

/*
Logger is the structure to use to produce logs.
Country is mandatory.
It is best to provide an app version to help in correlate logs with application upgrade.
UseCase is for identifying the app that is producing the logs.
*/
type Logger struct {
	Country    string
	Currency   string
	UseCase    string
	ShowDebug  bool
	AppVersion string
	logger     *zap.Logger
}

/*
Parameters is the structure to use them in logs.
*/
type Parameters struct {
	OrangeTransactionID           string     `json:"orangeTransactionId,omitempty"`
	LastUpdate                    string     `json:"lastUpdate,omitempty"`
	ServiceLatency                int64      `json:"serviceLatency,omitempty"`
	WalletNumber                  string     `json:"walletNumber,omitempty"`
	OrangeUserID                  string     `json:"orangeUserId,omitempty"`
	Msg                           string     `json:"message,omitempty"`
	TagStartTime                  string     `json:"tagStartTime,omitempty"`
	TagEndTime                    string     `json:"tagEndTime,omitempty"`
	DbrefStartTime                string     `json:"dbrefStartTime,omitempty"`
	DbrefEndTime                  string     `json:"dbrefEndTime,omitempty"`
	RuleIterationStartTime        string     `json:"ruleIterationStartTime,omitempty"`
	RuleIterationEndTime          string     `json:"ruleIterationEndTime,omitempty"`
	TagName                       string     `json:"tagName,omitempty"`
	TagID                         int        `json:"tagId,omitempty"`
	TransactionTagCount           *int64     `json:"txnTagCount,omitempty"`
	Frequency                     *int       `json:"frequency,omitempty"`
	TransactionLag                *int       `json:"transactionLag,omitempty"`
	TransactionTagLatency         int64      `json:"transactionTagLatency,omitempty"`
	DbrefLatency                  int64      `json:"dbrefLatency,omitempty"`
	RuleIterationTime             int64      `json:"ruleIterationLatency,omitempty"`
	TechnicalLatency              int64      `json:"technicalLatency,omitempty"`
	StartConsumeTime              string     `json:"startConsumeTime,omitempty"`
	EndConsumeTime                string     `json:"endConsumeTime,omitempty"`
	EffectedUserID                string     `json:"effectedUserID,omitempty"`
	FieldName                     string     `json:"fieldName,omitempty"`
	CompiFieldsSearchStartTime    *time.Time `json:"compiFieldsSearchStartTime,omitempty"`
	CompiFieldsSearchEndTime      *time.Time `json:"compiFieldsSearchEndTime,omitempty"`
	CompiFieldsSearchLatency      float64    `json:"compiFieldsSearchLatency,omitempty"`
	CompiCreatedByMsisdnStartTime *time.Time `json:"compiCreatedByMsisdnStartTime,omitempty"`
	CompiCreatedByMsisdnEndTime   *time.Time `json:"compiCreatedByMsisdnEndTime,omitempty"`
	CompiCreatedByMsisdnCount     *int64     `json:"compiCreatedByMsisdnCount,omitempty"`
	CompiCreatedByMsisdnLatency   float64    `json:"compiCreatedByMsisdnLatency,omitempty"`
	CompiNomadFieldsStartTime     *time.Time `json:"compiNomadFieldsStartTime,omitempty"`
	CompiNomadFieldsEndTime       *time.Time `json:"compiNomadFieldsEndTime,omitempty"`
	CompiNomadFieldsCount         *int64     `json:"compiNomadFieldsCount,omitempty"`
	CompiNomadFieldsLatency       float64    `json:"compiNomadFieldsLatency,omitempty"`
}

const (
	stdout = "stdout"
)

func (log *Logger) newLogger() {
	// niveau mini de log : doit-on afficher les traces de debug ou pas
	dyn := zap.NewAtomicLevel()
	dyn.SetLevel(zap.InfoLevel)
	if log.ShowDebug {
		dyn.SetLevel(zap.DebugLevel)
	}

	var outLogFile, outLogDir string
	dir, err := GetBinDir()
	if err != nil {
		outLogFile = stdout
	} else {
		outLogDir = dir + "/logs"
		outLogFile = outLogDir + "/" + path.Base(os.Args[0]) + ".log"
		err = os.Mkdir(outLogDir, 0755)
		if err != nil && os.IsNotExist(err) {
			outLogFile = stdout
		} else {
			file, err := os.OpenFile(outLogFile, os.O_RDONLY|os.O_CREATE, 0666)
			if err != nil {
				outLogFile = stdout
			}
			file.Close()
		}
	}

	w := zapcore.AddSync(&lumberjack.Logger{
		Filename:   outLogFile,
		MaxSize:    50, // megabytes
		MaxBackups: 5,
	})
	core := zapcore.NewCore(
		zapcore.NewJSONEncoder(zapcore.EncoderConfig{
			TimeKey:        "dateTime",
			LevelKey:       "level",
			NameKey:        "logger",
			MessageKey:     "returnedStatus",
			StacktraceKey:  "stacktrace",
			EncodeLevel:    zapcore.LowercaseLevelEncoder,
			EncodeTime:     zapcore.ISO8601TimeEncoder,
			EncodeDuration: zapcore.SecondsDurationEncoder,
			EncodeCaller:   zapcore.ShortCallerEncoder,
		}),
		w,
		dyn,
	)

	var initialFields []zap.Field

	initialFields = append(initialFields, zap.String("country", log.Country),
		zap.String("currency", log.Currency),
		zap.String("useCase", log.UseCase),
		zap.String("appVersion", log.AppVersion))

	core = core.With(initialFields)

	log.logger = zap.New(core)
	defer func() {
		err = log.logger.Sync()
		if err != nil {
			log.logger.Info("Failed to sync logger.")
		}
	}()
}

/*
WithSessionID is to be used to update logger and add a sessionId for all subsequent invocations.
The logger is cloned to avoid concurrency issues.
*/
func (log *Logger) WithSessionID(sessionID string) Logger {
	if log.logger == nil {
		log.newLogger()
	}
	log.logger = log.logger.With(zap.String("sessionID", sessionID))
	return *log

}

/*
Debug is for all logs that are for developers.
*/
func (log *Logger) Debug(msg string, fields ...zapcore.Field) {
	if log.logger == nil {
		log.newLogger()
	}
	log.logger.Debug(msg, fields...)
}

/*
Info is for all logs that are for informative purpose.
Incoming requests for example, or outgoing results.
*/
func (log *Logger) Info(msg string, fields ...zapcore.Field) {
	if log.logger == nil {
		log.newLogger()
	}
	log.logger.Info(msg, fields...)
}

/*
Infof is forall formatted logs that are for informative purpose.
Incomingrequests for example, or outgoing results.
*/
func (log *Logger) Infof(msg string, vars ...interface{}) {
	if log.logger == nil {
		log.newLogger()
	}
	logMsg := fmt.Sprintf(msg, vars...)
	log.Info(logMsg)
}

/*
Error level is for any error that is not recoverable : a retry will not solve the problem.
The administrator should have action to fix the problem.
It is for inconsistency in data for example.
*/
func (log *Logger) Error(msg string, err error, fields ...zapcore.Field) {
	if log.logger == nil {
		log.newLogger()
	}
	fields = append(fields, zap.NamedError("errorDetails", err))
	log.logger.Error(msg, fields...)
}

/*
Fatal is for recoverable errors. Executable should be stopped after this error.
It is itended to cover cases like network error, database overload, lack of resources, etc.
*/
func (log *Logger) Fatal(msg string, err error, fields ...zapcore.Field) {
	if log.logger == nil {
		log.newLogger()
	}
	fields = append(fields, zap.NamedError("errorDetails", err))
	log.logger.Fatal(msg, fields...)
}

func (log *Logger) Warning(msg string, err error, fields ...zapcore.Field) {
	if log.logger == nil {
		log.newLogger()
	}
	fields = append(fields, zap.NamedError("errorDetails", err))
	log.logger.Warn(msg, fields...)
}

/*
RedoJSON is the structure that is passed by kafka, it is necessary for formating incoming message logs.
*/
type (
	//RedoJSON : RedoJSON struct
	RedoJSON struct {
		striim Striim
		Data   map[string]string `json:"data,omitempty"`
		Before map[string]string `json:"before,omitempty"`
	}
	//Striim : Striim struct
	Striim struct {
		DateTime      string `json:"dateTime,omitempty"`
		OperationName string `json:"OperationName,omitempty"`
		TableName     string `json:"TableName,omitempty"`
		SeqID         string `json:"SeqID,omitempty"`
		RowID         string `json:"RowID,omitempty"`
	}
)

/*
IncomingKafkaMessage is to be used to trace the incoming kafka messages at beginning of treatments.
*/
func (log *Logger) IncomingKafkaMessage(kafkaMsg []byte, msg string) {
	var redos []RedoJSON
	err := json.Unmarshal(kafkaMsg, &redos)
	if err != nil {
		log.Debug(msg, zap.String("eventType", "incoming request"), zap.String("peer", "kafka"), zap.String("parameters", string(kafkaMsg)))
	} else {
		log.Debug(msg, zap.String("eventType", "incoming request"), zap.String("peer", "kafka"), zap.Any("parameters", redos))
	}
}

const (
	//StatusOk is for the case where all processing of message is ok, it has
	//been parsed, inserted into db and acknowledged to kafka.
	StatusOk string = "ok"
	//StatusKoDataStrongInconsistency is for the case where the data provided by Kafka are not
	//consistent, it cannot be written in base but because the error cannot be recovered,
	//the message has been acknowledged to kafka.
	StatusKoDataStrongInconsistency = "strong inconsistency in message"
	//StatusOkDataAlreadyPresent is for the case where the data provided by Kafka have been
	//already written in base. The message has been acknowledged to kafka.
	StatusOkDataAlreadyPresent = "data is already in database"
	//StatusOkNothingToBeDone is when treatment is ok but nothing has to be written in base.
	//Kafka has been acknowledged.
	StatusOkNothingToBeDone = "nothing to be done"
	//StatusKoTechnicalError is when some error was raised during treatment.
	//The db writing has not been done nor the kafka acknowledged.
	StatusKoTechnicalError = "technical error"
)

/*
OutgoingKafkaMessageOk is to be used to trace the status OK at the end of treatments.
*/
func (log *Logger) OutgoingKafkaMessageOk(param []byte, msg string) {
	var params Parameters
	json.Unmarshal(param, &params)
	log.Info("", zap.String("eventType", msg), zap.String("returnedStatus", StatusOk), zap.String("peer", "kafka"), zap.Any("parameters", params))
}

/*
OutgoingKafkaMessageOk1 is to be used to trace the status OK at the end of treatments.
*/
func (log *Logger) OutgoingKafkaMessageOk1(param []byte, msg string, peer string) {
	var params Parameters
	json.Unmarshal(param, &params)
	log.Info("", zap.String("eventType", msg), zap.String("returnedStatus", StatusOk), zap.String("peer", peer), zap.Any("parameters", params))
}

/*
OutgoingKafkaMessageStrongInconsistency is to be used to trace the end of treatments with strong inconsistency.
*/
func (log *Logger) OutgoingKafkaMessageStrongInconsistency(msg string, err error) {
	log.Error(msg, err)
	log.Info(msg, zap.String("eventType", "outgoing response"), zap.String("returnedStatus", StatusKoDataStrongInconsistency), zap.String("peer", "kafka"), zap.String("parameters", string(msg)))
}

/*
OutgoingKafkaDataAlreadyPresent is to be used to trace the end of treatments when data is already in db.
*/
func (log *Logger) OutgoingKafkaDataAlreadyPresent(param []byte, msg string) {
	var params Parameters
	json.Unmarshal(param, &params)
	log.Error(msg, nil, zap.String("eventType", "duplicate record - skipping message"), zap.String("returnedStatus", StatusOkDataAlreadyPresent), zap.String("peer", "kafka"), zap.Any("parameters", params))
	// log.Info("", zap.String("eventType", msg), zap.Bool("success", true), zap.String("returnedStatus", StatusOkDataAlreadyPresent), zap.String("peer", "kafka"), zap.Any("parameters", params))
}

/*
OutgoingKafkaNothingToDo is to be used to trace the end of treatments when no data has to be written in db.
*/
func (log *Logger) OutgoingKafkaNothingToDo(param []byte, msg string) {
	var params Parameters
	json.Unmarshal(param, &params)
	log.Debug(msg, zap.String("eventType", "outgoing response"), zap.String("returnedStatus", StatusOkNothingToBeDone), zap.String("peer", "kafka"), zap.Any("parameters", params))
}

/*
OutgoingKafkaMessageTechnicalError : OutgoingKafkaMessageTechnicalError is to be used to trace the status ko at the end of treatments.
A problem has been raised and the treatement has stopped.
*/
func (log *Logger) OutgoingKafkaMessageTechnicalError(msg string, err error) {
	log.Error(msg, err)
	log.Info(msg, zap.String("eventType", "outgoing response"), zap.String("returnedStatus", StatusKoTechnicalError), zap.String("peer", "kafka"), zap.String("parameters", string(msg)))
}

/*
InconsistentKafkaMessage is for error in the message treated.
These errors logs should be analysed by ops to identify changes in tango database queries.
*/
func (log *Logger) InconsistentKafkaMessage(msg string, err error) {
	log.Error(msg, err)
}

// GetBinDir : GetBinDir function
func GetBinDir() (string, error) {
	dir, err := filepath.Abs(filepath.Dir(os.Args[0]))
	if err != nil {
		return ".", err
	}
	return dir, err
}
