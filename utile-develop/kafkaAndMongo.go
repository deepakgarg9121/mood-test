package utile

import (
	"context"
	"crypto/cipher"
	"crypto/des"
	"encoding/base64"
	"fmt"
	"os"
	"path"
	"strings"
	"sync"
	"time"

	"github.com/confluentinc/confluent-kafka-go/kafka"
	"go.mongodb.org/mongo-driver/mongo"
	"go.mongodb.org/mongo-driver/mongo/options"
	"go.mongodb.org/mongo-driver/mongo/readpref"
	"go.mongodb.org/mongo-driver/mongo/writeconcern"
	"go.mongodb.org/mongo-driver/x/bsonx"
	"go.uber.org/zap"
)

var onceEnsureIndexTransaction sync.Once

// OpenKafkaConnection : OpenKafkaConnection func
func OpenKafkaConnection(kafkaConfigMap kafka.ConfigMap, country, currency string, caPath, certPath, keyPath string, logger Logger) (*kafka.Consumer, error) {
	groupID := path.Base(os.Args[0]) + "_" + country + "_" + currency
	clientID := country + "-databroker-" + path.Base(os.Args[0])

	topics := []string{"tango_" + country + "_" + currency, "tango_" + country + "_" + currency + "_recovery"}

	kafkaConfigMap["client.id"] = clientID
	kafkaConfigMap["group.id"] = groupID
	if caPath != "" && certPath != "" && keyPath != "" {
		kafkaConfigMap["security.protocol"] = "SSL"
		kafkaConfigMap["ssl.ca.location"] = caPath
		kafkaConfigMap["ssl.certificate.location"] = certPath
		kafkaConfigMap["ssl.key.location"] = keyPath
	}
	/*if ssl.SslPassword != "" {
		kafkaConfigMap["ssl.key.password"] = ssl.SslPassword
	}*/
	//fmt.Printf("#%v\n",kafkaConfigMap)
	consumer, err := kafka.NewConsumer(&kafkaConfigMap)

	if err != nil {
		return nil, err
	}

	err = consumer.SubscribeTopics(topics, nil)
	if err != nil {
		return nil, err
	}

	/*	// watch errors
		go func() {
			for err := range consumer.Errors() {
				logger.Fatal("Kafka error", err)
				return
			}
		}()

		// watch notifications
		go func() {
			for note := range consumer.Notifications() {
				logger.Debug(fmt.Sprintf("Rebalanced"))
				for topic, partitions := range note.Claimed {
					for partition := range partitions {
						logger.Debug(fmt.Sprintf("Claimed partition [%s/%d]", topic, partition))
					}
				}
				for topic, partitions := range note.Released {
					for partition := range partitions {
						logger.Debug(fmt.Sprintf("Released partition [%s/%d]", topic, partition))
					}
				}
			}
		}()*/
	logger.Debug(fmt.Sprintf("Connected to kafka broker for client %s with groupId %s and topic %s ", clientID, groupID, strings.Join(topics[:], ",")))

	return consumer, nil
}

// OpenMongoConnection : OpenMongoConnection func
func OpenMongoConnection(mongoURL, mongoUserName, mongoPassword, database, collection, indexKey string, unique bool, logger Logger, prefType string) *mongo.Client {
	// Ouverture de la connection en base
	//This will establish one or more connections with the cluster of servers defined by the url parameter.

	ctx, cancel := context.WithTimeout(context.Background(), 10*time.Second)
	defer cancel()

	option := options.Client().ApplyURI(mongoURL)
	//Decrypting encrypted credentials
	if mongoUserName != "" && mongoPassword != "" {
		key := "mood-key"
		cb, err := des.NewCipher([]byte(key))
		if err != nil {
			logger.Error("Unable to generate new des key for decryption of mongo username and password", err)
		}

		auth := options.Credential{
			AuthSource: "admin",
			Username:   decrypt(cb, mongoUserName),
			Password:   decrypt(cb, mongoPassword),
		}
		option = option.SetAuth(auth)
	}

	// pour déclencher un retour sur les opérations (attention, c'est consomateur de temps)
	option = option.SetWriteConcern(writeconcern.New(writeconcern.WMajority()))
	// Reads may not be entirely up-to-date, but they will always see the
	// history of changes moving forward, the data read will be consistent
	// across sequential queries in the same session, and modifications made
	// within the session will be observed in following queries (read-your-writes).
	if prefType == "primary" {
		option = option.SetReadPreference(readpref.PrimaryPreferred())
	} else {
		option = option.SetReadPreference(readpref.Secondary())
	}

	client, err := mongo.Connect(ctx, option)
	if err != nil {
		logger.Error("Couldn't connect to mongo.", err)
	}

	// Vérification des index
	err = ensureIndexTransaction(client, database, collection, indexKey, unique)
	if err != nil {
		logger.Fatal("Error Mongo on create index", err)
	}

	return client
}

// OpenMongoConnectionWithoutUniqueAndBackground : OpenMongoConnectionWithoutUniqueAndBackground func
func OpenMongoConnectionWithoutUniqueAndBackground(mongoURL, mongoUserName, mongoPassword, database, collection, indexKey string, logger Logger, prefType string) *mongo.Client {
	// Ouverture de la connection en base
	//This will establish one or more connections with the cluster of servers defined by the url parameter.

	ctx, cancel := context.WithTimeout(context.Background(), 10*time.Second)
	defer cancel()

	option := options.Client().ApplyURI(mongoURL)
	//Decrypting encrypted credentials
	if mongoUserName != "" && mongoPassword != "" {
		key := "mood-key"
		cb, err := des.NewCipher([]byte(key))
		if err != nil {
			logger.Error("Unable to generate new des key for decryption of mongo username and password", err)
		}

		auth := options.Credential{
			AuthSource: "admin",
			Username:   decrypt(cb, mongoUserName),
			Password:   decrypt(cb, mongoPassword),
		}
		option = option.SetAuth(auth)
	}

	// pour déclencher un retour sur les opérations (attention, c'est consomateur de temps)
	option = option.SetWriteConcern(writeconcern.New(writeconcern.WMajority()))
	// Reads may not be entirely up-to-date, but they will always see the
	// history of changes moving forward, the data read will be consistent
	// across sequential queries in the same session, and modifications made
	// within the session will be observed in following queries (read-your-writes).
	if prefType == "primary" {
		option = option.SetReadPreference(readpref.PrimaryPreferred())
	} else {
		option = option.SetReadPreference(readpref.Secondary())
	}

	client, err := mongo.Connect(ctx, option)
	if err != nil {
		logger.Error("Couldn't connect to mongo.", err)
	}

	// Vérification des index
	err = ensureIndexTransactionWithoutUniqueAndBackground(client, database, collection, indexKey)
	if err != nil {
		logger.Fatal("Error Mongo on create index", err)
	}

	return client
}

// CatchPanicSignalFunc : CatchPanicSignalFunc func
func CatchPanicSignalFunc(errorChan chan struct{}, once *sync.Once) {
	logger.Debug("In Catch Panic Signal Func")
	if r := recover(); r != nil {
		err := fmt.Errorf("%v", r)
		logger.Error("Panic signal catched ", err, zap.Stack("stack"))
		once.Do(func() {
			close(errorChan)
		})
		return
	}
}

func ensureIndexTransaction(client *mongo.Client, database, collection, indexKey string, unique bool) error {
	//Single session does not allow concurrent processing, therefore multiple sessions are usually required
	//The quickest way to get another session is to copy an existing one. Make sure that you close it after use:
	col := client.Database(database).Collection(collection)
	mod := mongo.IndexModel{
		Keys:    bsonx.Doc{{Key: indexKey, Value: bsonx.Int32(1)}},
		Options: options.Index().SetUnique(unique).SetBackground(true),
	}

	ctx, cancel := context.WithTimeout(context.Background(), 15*time.Second)
	defer cancel()

	var errEnsureIndexTransaction error
	onceEnsureIndexTransaction.Do(
		func() {
			_, errEnsureIndexTransaction = col.Indexes().CreateOne(ctx, mod)
		},
	)
	return errEnsureIndexTransaction

}

func ensureIndexTransactionWithoutUniqueAndBackground(client *mongo.Client, database, collection, indexKey string) error {
	//Single session does not allow concurrent processing, therefore multiple sessions are usually required
	//The quickest way to get another session is to copy an existing one. Make sure that you close it after use:
	col := client.Database(database).Collection(collection)

	mod := mongo.IndexModel{
		Keys: bsonx.Doc{{Key: indexKey, Value: bsonx.Int32(1)}},
	}

	ctx, cancel := context.WithTimeout(context.Background(), 15*time.Second)
	defer cancel()

	var errEnsureIndexTransaction error
	onceEnsureIndexTransaction.Do(
		func() {
			_, errEnsureIndexTransaction = col.Indexes().CreateOne(ctx, mod)
		},
	)
	return errEnsureIndexTransaction

}

//GetBufferedMessageChannel : GetBufferedMessageChannel func
/*func GetBufferedMessageChannel(consumer *cluster.Consumer, maxGoroutines int, logger Logger) chan *sarama.ConsumerMessage {

	am := make(chan *sarama.ConsumerMessage, maxGoroutines)

	go func() {
		for {
			t := <-consumer.Messages()
			//logger.Debug(fmt.Sprintf("Receiving message from Kafka "))
			am <- t
		}
	}()

	return am
}
*/
func encrypt(cb cipher.Block, message string) string {
	in := []byte(message)
	appendCount := 8 - len(in)%8
	for i := 0; i < appendCount; i++ {
		in = append(in, '.')
	}
	out := make([]byte, len(in))
	fmt.Println(string(in))
	for i := 0; i < len(in); i = i + 8 {
		cb.Encrypt(out[i:i+8], in[i:i+8])
	}

	return base64.StdEncoding.EncodeToString(out)
}

func decrypt(cb cipher.Block, encryptedMessage string) string {
	in, err := base64.StdEncoding.DecodeString(encryptedMessage)
	if err != nil {
		logger.Error("Unable to decode base64 encoded encrypted data for mongo credentials.", err)
	}
	out := make([]byte, len(in))

	for i := 0; i < len(in); i = i + 8 {
		cb.Decrypt(out[i:i+8], in[i:i+8])
	}
	return strings.ReplaceAll(string(out), ".", "")
}

/**************************
	ERRORS
 *************************/

// TechnicalErrorInterface : TechnicalErrorInterface Interface
type TechnicalErrorInterface interface {
	GetError() error
	GetMsg() string
	IsTechnical() bool
}

// TechnicalError : TechnicalError struct
type TechnicalError struct {
	Error error
	Msg   string
}

// GetError : GetError func
func (p TechnicalError) GetError() error {
	return p.Error
}

// GetMsg : GetMsg func
func (p TechnicalError) GetMsg() string {
	return p.Msg
}

// IsTechnical : IsTechnical func
func (p TechnicalError) IsTechnical() bool {
	return true
}

// DataError : DataError struct
type DataError struct {
	Error error
	Msg   string
}

// GetError : GetError func
func (p DataError) GetError() error {
	return p.Error
}

// GetMsg : GetMsg func
func (p DataError) GetMsg() string {
	return p.Msg
}

// IsTechnical : IsTechnical func
func (p DataError) IsTechnical() bool {
	return false
}
