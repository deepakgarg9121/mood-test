package utile

import (
	"path/filepath"
	"strings"

	"github.com/spf13/viper"
)

/*
SslDetails is used to get the value of ssl certificates used for kafka connection
*/
type SslDetails struct {
	CaPath      string
	CertPath    string
	KeyPath     string
	SslPassword string
}

// ReadConfiguration : Read configuration file into the structure overwriting the default values
func ReadConfiguration(configPath string) (*viper.Viper, error) {

	defaults := map[string]interface{}{
		"kafkaDetails": map[string]interface{}{
			"kafkaBrokerList":        "127.0.0.1",
			"kafkaSessionTimeout":    60000,
			"kafkaChannelEnable":     true,
			"kafkaRebalanceEnable":   true,
			"kafkaPartitionEof":      true,
			"kafkaAutoCommit":        false,
			"kafkaOffsetReset":       "earliest",
			"kafkaHeartbeatInterval": 3000,
		},
		"country":        "mg",
		"currency":       "mga",
		"currencyFactor": 100.0,
		"debug":          false,
		"mongoDetails": map[string]string{
			"mongoURL":        "mongodb://127.0.0.1:27017",
			"mongodbUsername": "",
			"mongodbPassword": "",
		},
		"maxRoutines": 1,
		"caPath":      "",
		"certPath":    "",
		"keyPath":     "",
	}
	v := viper.New()
	for key, value := range defaults {
		// fmt.Println("value is---->",key,value)
		v.SetDefault(key, value)
	}

	configDir, err := filepath.Abs(filepath.Dir(configPath))
	if err != nil {
		return nil, err
	}
	configFile := strings.Split(filepath.Base(configPath), ".")[0]

	v.SetConfigName(configFile)
	v.AddConfigPath(configDir)
	v.AutomaticEnv()
	v.ReadInConfig()

	return v, nil
}
