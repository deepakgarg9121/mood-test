package utile

import (
	"flag"

	"net/http"

	"github.com/prometheus/client_golang/prometheus"
	"github.com/prometheus/client_golang/prometheus/promhttp"
)

type monitoring struct {
	Label                           string
	CounterAlreadyInDatabase        *prometheus.CounterVec
	CounterInsertDatabase           *prometheus.CounterVec
	CounterKafkaMessage             *prometheus.CounterVec
	CounterDbrefRuleIteration       *prometheus.CounterVec
	CounterCompiFieldsIteration     *prometheus.CounterVec
	CounterErrWriting               *prometheus.CounterVec
	CounterManaged                  *prometheus.CounterVec
	CounterErrorFindOne             *prometheus.CounterVec
	CounterWritingTime              *prometheus.GaugeVec
	CounterFindOneTime              *prometheus.GaugeVec
	CounterStatusService            *prometheus.CounterVec
	CounterUpdate                   *prometheus.CounterVec
	CounterInsert                   *prometheus.CounterVec
	CounterDelete                   *prometheus.CounterVec
	CounterIDMissing                *prometheus.CounterVec
	CounterNotToWriteInDatabase     *prometheus.CounterVec
	CounterErrUpsert                *prometheus.CounterVec
	CounterMultipleInsert           *prometheus.CounterVec
	CounterFind                     *prometheus.CounterVec
	CounterWriting                  *prometheus.CounterVec
	CounterDupKey                   *prometheus.CounterVec
	CounterErrSocket                *prometheus.CounterVec
	CounterConsumeMessageGoRoutines *prometheus.CounterVec
	CounterRetryCount               *prometheus.CounterVec
}

//Monitor : Initialisation de la structure
var Monitor = monitoring{
	"",
	prometheus.NewCounterVec(prometheus.CounterOpts{Name: "already_in_database", Help: "The total number of kafka messages that are already in database."}, []string{"component"}),
	prometheus.NewCounterVec(prometheus.CounterOpts{
		Name: "insertion_database",
		Help: "The total number of kafka messages registered in MongoDB (update or insert a new element.",
	}, []string{"component"}),
	prometheus.NewCounterVec(prometheus.CounterOpts{
		Name: "Kafka_messages",
		Help: "The number of kafaka messages.",
	}, []string{"component"}),
	prometheus.NewCounterVec(prometheus.CounterOpts{
		Name: "total_dbref_iterations",
		Help: "The number of iterations over dbref rules.",
	}, []string{"component"}),
	prometheus.NewCounterVec(prometheus.CounterOpts{
		Name: "total_compifields_iterations",
		Help: "The number of iterations over compifields rules.",
	}, []string{"component"}),
	prometheus.NewCounterVec(prometheus.CounterOpts{
		Name: "mongodb_writing_error",
		Help: "The total number of writing errors in MongoDB.",
	}, []string{"component"}),
	prometheus.NewCounterVec(prometheus.CounterOpts{
		Name: "messages_managed",
		Help: "The total number of messages managed.",
	}, []string{"component"}),
	prometheus.NewCounterVec(prometheus.CounterOpts{
		Name: "error_find_one",
		Help: "The total number of errors due to mongodb.",
	}, []string{"component"}),
	prometheus.NewGaugeVec(prometheus.GaugeOpts{
		Name: "writing_time",
		Help: "The time require to write a message in mongoDB.",
	}, []string{"component"}),
	prometheus.NewGaugeVec(prometheus.GaugeOpts{
		Name: "find_one_time",
		Help: "The time require to find a transaction in mongoDB.",
	}, []string{"component"}),
	prometheus.NewCounterVec(prometheus.CounterOpts{
		Name: "counter_status_service",
		Help: "Value = 1 when the service is lauch",
	}, []string{"component"}),
	prometheus.NewCounterVec(prometheus.CounterOpts{
		Name: "Str",
		Help: "the number of datas updated in MongoDB",
	}, []string{"component"}),
	prometheus.NewCounterVec(prometheus.CounterOpts{
		Name: "counter_insert",
		Help: "the number of datas insert in MongoDB",
	}, []string{"component"}),
	prometheus.NewCounterVec(prometheus.CounterOpts{
		Name: "counter_delete",
		Help: "the number of datas delete in MongoDB",
	}, []string{"component"}),
	prometheus.NewCounterVec(prometheus.CounterOpts{
		Name: "counter_Id_missing",
		Help: "The number of kafka messages without userId",
	}, []string{"component"}),
	prometheus.NewCounterVec(prometheus.CounterOpts{
		Name: "counter_not_to_write_in_mongoDB",
		Help: "The number of kafka messages not to write in logs",
	}, []string{"component"}),
	prometheus.NewCounterVec(prometheus.CounterOpts{
		Name: "counter_error_upsert",
		Help: "The number of kafka messages upsert ending with an error",
	}, []string{"component"}),
	prometheus.NewCounterVec(prometheus.CounterOpts{
		Name: "counter_multiple_insert",
		Help: "Error causing by multiple insert",
	}, []string{"component"}),
	prometheus.NewCounterVec(prometheus.CounterOpts{
		Name: "counter_find",
		Help: "Tentative number to find a document in mongoDB",
	}, []string{"component"}),
	prometheus.NewCounterVec(prometheus.CounterOpts{
		Name: "counter_writing",
		Help: "attemps writing",
	}, []string{"component"}),
	prometheus.NewCounterVec(prometheus.CounterOpts{
		Name: "counter_duplicate_key",
		Help: "Error duplicate key",
	}, []string{"component"}),
	prometheus.NewCounterVec(prometheus.CounterOpts{
		Name: "counter_err_socket",
		Help: "Error socket",
	}, []string{"component"}),
	prometheus.NewCounterVec(prometheus.CounterOpts{
		Name: "counter_consume_message_go_routines",
		Help: "Total Go routines to consume kafka message",
	}, []string{"component"}),
	prometheus.NewCounterVec(prometheus.CounterOpts{
		Name: "counter_retry_count",
		Help: "Total retry counts while consuming kafka message",
	}, []string{"component"}),
}

//Enregistre les valeurs des éléments de la structure et les envoient à l'adresse du port demandé (ex localhost:8087/metrics)
func (m monitoring) Start(port string, logger Logger) {
	addr := flag.String("listen-address", port, "The address to listen on for HTTP requests.")

	prometheus.MustRegister(m.CounterAlreadyInDatabase)
	prometheus.MustRegister(m.CounterInsertDatabase)
	prometheus.MustRegister(m.CounterKafkaMessage)
	prometheus.MustRegister(m.CounterDbrefRuleIteration)
	prometheus.MustRegister(m.CounterCompiFieldsIteration)
	prometheus.MustRegister(m.CounterErrWriting)
	prometheus.MustRegister(m.CounterManaged)
	prometheus.MustRegister(m.CounterErrorFindOne)
	prometheus.MustRegister(m.CounterWritingTime)
	prometheus.MustRegister(m.CounterFindOneTime)
	prometheus.MustRegister(m.CounterUpdate)
	prometheus.MustRegister(m.CounterInsert)
	prometheus.MustRegister(m.CounterDelete)
	prometheus.MustRegister(m.CounterIDMissing)
	prometheus.MustRegister(m.CounterNotToWriteInDatabase)
	prometheus.MustRegister(m.CounterErrUpsert)
	prometheus.MustRegister(m.CounterMultipleInsert)
	prometheus.MustRegister(m.CounterFind)
	prometheus.MustRegister(m.CounterWriting)
	prometheus.MustRegister(m.CounterDupKey)
	prometheus.MustRegister(m.CounterErrSocket)
	prometheus.MustRegister(m.CounterConsumeMessageGoRoutines)
	prometheus.MustRegister(m.CounterRetryCount)

	prometheus.MustRegister(m.CounterStatusService)

	http.Handle("/metrics", promhttp.Handler())
	err := http.ListenAndServe(*addr, nil)
	if err != nil {
		logger.Error("problem http.ListenAndServe", err)
	}
}

//Enregistre le label, incrémente le compteur qui indique que le broker est en route et appelle la fonction Start
func (m *monitoring) InitPrometheus(exeName string, port string, logger Logger) {
	m.Label = exeName
	m.IncCounterBrokerStart()
	go m.Start(port, logger)
}

/////////////////////////////////////
/// Incrémentation des compteurs
////////////////////////////////////

func (m *monitoring) IncCounterAlreadyInDatabase() {
	m.CounterAlreadyInDatabase.WithLabelValues(m.Label).Inc()
}

func (m *monitoring) IncCounterInsertDatabase() {
	m.CounterInsertDatabase.WithLabelValues(m.Label).Inc()
}

func (m *monitoring) IncCounterKafkaMessage() {
	m.CounterKafkaMessage.WithLabelValues(m.Label).Inc()
}

func (m *monitoring) IncCounterDbrefRuleIteration() {
	m.CounterDbrefRuleIteration.WithLabelValues(m.Label).Inc()
}

func (m *monitoring) IncCounterCompiFieldsIteration() {
	m.CounterCompiFieldsIteration.WithLabelValues(m.Label).Inc()
}

func (m *monitoring) IncCounterErrWriting() {
	m.CounterErrWriting.WithLabelValues(m.Label).Inc()
}

func (m *monitoring) IncCounterManaged() {
	m.CounterManaged.WithLabelValues(m.Label).Inc()
}

func (m *monitoring) IncCounterErrorFindOne() {
	m.CounterErrorFindOne.WithLabelValues(m.Label).Inc()
}

func (m *monitoring) IncCounterWritingTime(set float64) {
	m.CounterWritingTime.WithLabelValues(m.Label).Set(set)
}

func (m *monitoring) IncCounterFindOneTime(set float64) {
	m.CounterFindOneTime.WithLabelValues(m.Label).Set(set)
}

func (m *monitoring) IncCounterBrokerStart() {
	m.CounterStatusService.WithLabelValues(m.Label).Inc()
}

func (m *monitoring) IncCounterUpdate() {
	m.CounterUpdate.WithLabelValues(m.Label).Inc()
}

func (m *monitoring) IncCounterInsert() {
	m.CounterInsert.WithLabelValues(m.Label).Inc()
}

func (m *monitoring) IncCounterDelete() {
	m.CounterDelete.WithLabelValues(m.Label).Inc()
}

func (m *monitoring) IncCounterIDMissing() {
	m.CounterIDMissing.WithLabelValues(m.Label).Inc()
}

func (m *monitoring) IncCounterNotToWrite() {
	m.CounterNotToWriteInDatabase.WithLabelValues(m.Label).Inc()
}

func (m *monitoring) IncCounterErrUpsert() {
	m.CounterErrUpsert.WithLabelValues(m.Label).Inc()
}

func (m *monitoring) IncCounterMultipleInsert() {
	m.CounterMultipleInsert.WithLabelValues(m.Label).Inc()
}

func (m *monitoring) IncCounterFind() {
	m.CounterFind.WithLabelValues(m.Label).Inc()
}

func (m *monitoring) IncCounterWriting() {
	m.CounterWriting.WithLabelValues(m.Label).Inc()
}

func (m *monitoring) IncCounterDupKey() {
	m.CounterDupKey.WithLabelValues(m.Label).Inc()
}

func (m *monitoring) IncCounterErrSocket() {
	m.CounterErrSocket.WithLabelValues(m.Label).Inc()
}

func (m *monitoring) IncCounterGoRoutines() {
	m.CounterConsumeMessageGoRoutines.WithLabelValues(m.Label).Inc()
}

func (m *monitoring) IncRetryCount() {
	m.CounterRetryCount.WithLabelValues(m.Label).Inc()
}
