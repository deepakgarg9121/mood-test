package utile

import (
	"fmt"
	"reflect"
	"regexp"
	"sort"
	"strconv"
	"time"

	"math/rand"

	"gopkg.in/mgo.v2/bson"
)

var (
	logger Logger
)

func init() {
	logger.newLogger()
}

// PortNumber : PortNumber int
type PortNumber int

// AddrAllInterfaces : AddrAllInterfaces method
func (p PortNumber) AddrAllInterfaces() string {
	return fmt.Sprintf("0.0.0.0:%v", p)
}

const (
	//PortNumberTransactionLog : PortNumberTransactionLog const
	PortNumberTransactionLog PortNumber = 10000 + iota //10001
	//PortNumberUserLog : PortNumberUserLog const
	PortNumberUserLog //10001
	//PortNumberCommission : PortNumberCommission const
	PortNumberCommission //10002
	//PortNumberSynchroDecoder : PortNumberSynchroDecoder const
	PortNumberSynchroDecoder //10003
	//PortNumberTransactionHistoryMonitoring : PortNumberTransactionHistoryMonitoring const
	PortNumberTransactionHistoryMonitoring PortNumber = 8082 + iota //8086
	//PortNumberUserHistoryMonitoring : PortNumberUserHistoryMonitoring const
	PortNumberUserHistoryMonitoring
	//PortNumberBalanceMonitoring : PortNumberBalanceMonitoring const
	PortNumberBalanceMonitoring
	//PortNumberTransactionLogMonitoring : PortNumberTransactionLogMonitoring const
	PortNumberTransactionLogMonitoring
	//PortNumberUserLogMonitoring : PortNumberUserLogMonitoring const
	PortNumberUserLogMonitoring
	//PortNumberTransactionHistory : PortNumberTransactionHistory const
	PortNumberTransactionHistory //8091
	//PortNumberUserHistory : PortNumberUserHistory const
	PortNumberUserHistory //8092
	//PortNumberBalance : PortNumberBalance const
	PortNumberBalance //8093
	//PortNumberCommissionMonitoring : PortNumberCommissionMonitoring const
	PortNumberCommissionMonitoring //8094

	PortNumberTxnTag           //8095
	PortNumberTxnTagMonitoring //8096

	PortNumberCompiTag           //8097
	PortNumberCompiTagMonitoring //8098
)

// AppendStrIfMissing : AppendStrIfMissing func
func AppendStrIfMissing(array []string, str string) []string {
	if str == "" {
		return array
	}
	for _, e := range array {
		if str == e {
			return array
		}
	}
	res := append(array, str)
	sort.Strings(res)
	return res
}

// AppendTimeIfMissing : AppendTimeIfMissing func
func AppendTimeIfMissing(array []time.Time, t time.Time) []time.Time {

	if t.IsZero() {
		return array
	}
	for _, e := range array {
		if t.UTC().Equal(e) {
			return array
		}
	}
	array = append(array, t)
	sort.Slice(array, func(i, j int) bool {
		return array[j].After(array[i])
	})
	return array
}

// AppendFloatsIfMissing : AppendFloatsIfMissing func
func AppendFloatsIfMissing(array []float64, arrayToAppend []float64) []float64 {
	var array2 []float64
	for _, elem := range arrayToAppend {
		array2 = AppendFloatIfMissing(array, elem)

	}

	return array2
}

// AppendFloatIfMissing : AppendFloatIfMissing func
func AppendFloatIfMissing(array []float64, t float64) []float64 {
	if t == 0 {
		return array
	}
	for _, e := range array {
		if t == e {
			return array
		}
	}
	return append(array, t)
}

// Random : Randomfunc
func Random(min, max int) int {
	return rand.Intn(max-min) + min
}

// IsValidNumeric : IsValidNumeric func
func IsValidNumeric(value string) bool {
	//TODO REGEX à revoir combien de chiffres avant . ?
	//	^                   # Start of string.
	//		[0-9]{1,19}             # Must have one number and less than 20 numbers
	//		(                   # Begin optional group.
	//	\.              # The decimal point, . must be escaped,
	//	# or it is treated as "any character".
	//		[0-9]{1,2}      # One or two numbers.
	//)?                  # End group, signify it's optional with ?
	//	$
	isNumeric := regexp.MustCompile(`^[+-]?[0-9]{1,19}(\.[0-9]{1,2})?$`).MatchString
	return isNumeric(value)
}

// ConvAmount : ConvAmount func
func ConvAmount(value string, currencyFactor float64) float64 {

	if value != "" && !IsValidNumeric(value) {
		logger.InconsistentKafkaMessage("Not a well formatted amount ["+value+"]", nil)
	} else {
		if value != "" && value != "0" {
			newAmount, err := strconv.ParseFloat(value, 64)

			if err != nil {
				logger.InconsistentKafkaMessage("Not a number", err)
			}
			//fmt.Printf("value conv %v", newAmount/100)
			return newAmount / currencyFactor
		}
	}

	return 0
}

// ConvertSortedDocElem : ConvertSortedDocElem func
func ConvertSortedDocElem(in map[string]interface{}) bson.D {
	keys := make([]string, 0, len(in))

	for key := range in {
		keys = append(keys, key)
	}

	sort.Strings(keys)

	sortedDocElem := make(bson.D, 0, len(keys))

	for _, key := range keys {
		sortedDocElem = append(sortedDocElem, bson.DocElem{
			Name:  key,
			Value: in[key],
		})
	}

	return sortedDocElem
}

// RemoveEmpty : RemoveEmpty func
func RemoveEmpty(m map[string]interface{}) map[string]interface{} {
	var res = make(map[string]interface{}, len(m))
	for k, v := range m {
		if v != "" {
			res[k] = v
		}
	}
	return res
}

// IsEmpty : IsEmpty func
func IsEmpty(object interface{}) bool {
	//First check normal definitions of empty
	if object == nil {
		return true
	} else if object == "" {
		return true
	} else if object == false {
		return true
	}

	//Then see if it's a struct
	if reflect.ValueOf(object).Kind() == reflect.Struct {
		// and create an empty copy of the struct object to compare against
		empty := reflect.New(reflect.TypeOf(object)).Elem().Interface()
		if reflect.DeepEqual(object, empty) {
			return true
		}
	}
	return false
}

// ConvertStringToDate : ConvertStringToDate func
func ConvertStringToDate(dateString string) time.Time {
	date, _ := time.Parse(time.RFC3339, dateString)
	/*if err != nil {
		// si la date n'est pas parsable, le retour sera une date nule, time.Time{}, ce qui va correspondre à 0001-01-01 00:00:00 +0000 UTC
		//logger.InconsistentKafkaMessage(dateString+" is not a date", err)
	}*/
	return date.UTC()
}

// ConvertAbsoluteDateStringToDate : ConvertStringToDate func
func ConvertAbsoluteDateStringToDate(dateString string) time.Time {
	date, _ := time.Parse(time.RFC3339, dateString)
	/*if err != nil {
		// si la date n'est pas parsable, le retour sera une date nule, time.Time{}, ce qui va correspondre à 0001-01-01 00:00:00 +0000 UTC
		//logger.InconsistentKafkaMessage(dateString+" is not a date", err)
	}*/
	return date
}

// ConvertStringToInt : ConvertStringToInt func
func ConvertStringToInt(fieldName string, intString string) int {
	i, err := strconv.Atoi(intString)
	if err != nil {
		// TODO: vérifier que le traitement est bien celui d'un arret ...
		logger.InconsistentKafkaMessage(fieldName+" is not an integer ["+intString+"]", err)
	}
	logger.Debug(fieldName + " " + intString)
	return i
}

/*func ConvertStringToHexValue(fieldName string, intString string) string {
	n := new(big.Int)
	n, ok := n.SetString(intString, 10)
	if !ok {
		logger.InconsistentKafkaMessage(fieldName+" is not an integer", nil)
	}
	return toHexInt(n)
}
func toHexInt(n *big.Int) string {
	return fmt.Sprintf("%x", n) // or %X or upper case
}*/

// AddLeadingZeros : AddLeadingZeros func
func AddLeadingZeros(intString string, pad string, plength int) string {
	for i := len(intString); i < plength; i++ {
		intString = pad + intString
	}
	return intString
}

// ComputeOrangeTransactionID : ComputeOrangeTransactionID func
func ComputeOrangeTransactionID(tangoTransferID string, country string, currency string) string {
	return country + "_" + currency + "_" + tangoTransferID
}

// ComputeCountryDBUniqueIdentifier : ComputeCountryDBUniqueIdentifier func
func ComputeCountryDBUniqueIdentifier(country string, currency string) string {
	return country + "_" + currency
}
