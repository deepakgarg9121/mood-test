module gitlab.com/deepakgarg9121/mood-test/myframe

go 1.12

require (
	github.com/InVisionApp/go-health v2.1.0+incompatible
	github.com/InVisionApp/go-logger v1.0.1 // indirect
	github.com/dgraph-io/dgo v1.0.0
	github.com/dgrijalva/jwt-go v3.2.0+incompatible
	github.com/golang/mock v1.1.1
	github.com/lib/pq v1.2.0
	github.com/onsi/gomega v1.7.0 // indirect
	github.com/opencensus-integrations/ocsql v0.1.4
	github.com/pkg/errors v0.8.1
	github.com/rs/cors v1.7.0
	github.com/sirupsen/logrus v1.4.2 // indirect
	github.com/stretchr/testify v1.4.0 // indirect
	go.opencensus.io v0.22.1 // indirect
	go.uber.org/atomic v1.4.0 // indirect
	go.uber.org/multierr v1.2.0 // indirect
	go.uber.org/zap v1.11.0
	google.golang.org/appengine v1.4.0 // indirect
	google.golang.org/grpc v1.23.0
)
