package config

import (
	"encoding/json"
	"fmt"
)

type AppConfig struct {
	AppName           string      `json:"AppName"`
	AppVersion        string      `json:"AppVersion"`
	ServerPort        string      `json:"ServerPort"`
	ApplicationConfig interface{} `json:"ApplicationConfig"`
}

func (this *AppConfig) String() string {
	str, err := json.Marshal(this)
	if err != nil {
		return "Could NOT Marshal APP config."
	}
	return string(str)
}

func (this *AppConfig) ShowConfig() string {

	s := fmt.Sprintf(
		"AppName      := %s\n"+
			"AppVersion   := %s\n"+
			"ServerPort   := %s\n"+
			"Log File     := %s\n"+
			"App Config   := %+v\n"+
			this.AppName,
		this.AppVersion,
		this.ServerPort,
		this.ApplicationConfig,
	)

	return s
}

// GlobalAppConfig is applicationconfig Singleton
var GlobalAppConfig = new(AppConfig)
