package config

import (
	"encoding/json"
	"fmt"
	"io/ioutil"
)

const DefaultConfFile = "conf/config.json"
const TestConfFile = "../conf/config.json"

type ConfigManager struct {
	ConfFile string
}

func (cm *ConfigManager) InitializeGlobalConfig(applicationConfig interface{}) {

	GlobalAppConfig.ApplicationConfig = applicationConfig
	cm.Initialize(cm.ConfFile, GlobalAppConfig)
	//log.Printf("Global Config is listed below: \n%+v", config.GlobalAppConfig)
	//log.Printf("Application Config is listed below: \n%+v", config.GlobalAppConfig.ApplicationConfig)
}

func (cm *ConfigManager) Initialize(filePath string, conf *AppConfig) {
	//log.Println("Initializing Application Config")
	file, err := ioutil.ReadFile(filePath)
	if err != nil {
		panic(fmt.Sprintf("Error loading App Config file %s \n %s", filePath, err))
	}
	err = json.Unmarshal(file, &conf)
	if err != nil {
		panic(fmt.Sprintf("Incorrect Json in %s \n %s", filePath, err))
	}
	//log.Println("Application Config Initialized")
}
