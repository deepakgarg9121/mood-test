package mongo

import (
	"context"
	"fmt"

	"github.com/pkg/errors"
	"go.mongodb.org/mongo-driver/mongo"
	"go.mongodb.org/mongo-driver/mongo/options"
)

// config from mongo db
type MDBConfig struct {
	URL    string `json:"URL"` // e.g. mongodb://localhost:27017
	DbName string `json:"DbName"`
}

// Validate checks that the configuration is valid.
func (c MDBConfig) Validate() error {
	if c.URL == "" {
		return errors.New("Host is required")
	}

	if c.DbName == "" {
		return errors.New("Db Name is required")
	}

	return nil
}

// DSN returns a Postgres driver compatible data source name.
func (c MDBConfig) Init() (*mongo.Database, error) {
	// Set client options
	clientOptions := options.Client().ApplyURI(c.URL)

	// Connect to MongoDB
	client, err := mongo.Connect(context.TODO(), clientOptions)

	if err != nil {
		return nil, err
	}
	db := client.Database(c.DbName)

	// Check the connection
	err = client.Ping(context.TODO(), nil)

	if err != nil {
		return nil, err
	}

	fmt.Println("Connected to MongoDB!")
	return db, nil
}
